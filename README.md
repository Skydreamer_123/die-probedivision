# Die Probedivision

Dieses Projekt ist eine Implementierung der Probedivision. Durch eine graphische Benutzeroberfläche kann der Benutzer verschiedene natürliche  Zahlen auf Primalität prüfen. Das Programm ermittelt, ob die eingegebene Zahl primär oder zusammengesetzt ist, welche Teiler die Zahl hat, wie viele Divisionen der Test benötigt und wie lange der Test benötigt hat. 

## Wie starte ich das Programm?
Um das Programm zu starten, müssen Sie die "die-probedivision.jar" Datei herunterladen und ausführen. Danach sollte sich ein Fenster öffnen.

## Was kann das Programm?
Das Programm soll das Laufzeitverhalten der Probedivsion zeigen. Es verdeutlicht, dass die Größe des kleinsten Primteilers ausschlaggebend für das Laufzeitverhalten ist. 
