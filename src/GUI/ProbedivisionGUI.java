package GUI;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.math.BigInteger;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import lib.JNumberField;
import test.PrimTest;

public class ProbedivisionGUI extends JFrame {
  // Anfang Attribute
  private JLabel lDieProbedivision = new JLabel();
  private JLabel lEingabe1 = new JLabel();
  private JLabel lZahl1 = new JLabel();
  private JNumberField gegebeneZahl = new JNumberField();
  private JLabel lBeispiele1 = new JLabel();
  private JLabel lBestcase = new JLabel();
  private JButton jButton1 = new JButton();
  private JLabel lWorstcase = new JLabel();
  private JButton jButton2 = new JButton();
  private JLabel lAusgabeText = new JLabel();
  private JButton checkZahl = new JButton();
  private JLabel lAnzahlanDivisionen = new JLabel();
  private JLabel lMersenneZahl = new JLabel();
  private JNumberField MersenneExponent = new JNumberField();
  private JButton checkMersenne = new JButton();
  private JLabel l2 = new JLabel();
  private JLabel l1 = new JLabel();
  private JLabel lBenoetigteZeit1 = new JLabel();
  private JLabel primfaktor = new JLabel();
  private JLabel isPrime = new JLabel();
  private JButton bBERECHNUNGABBRECHEN = new JButton();
  private JButton bBeenden = new JButton();

  private char checkUnicode;
  private String checkSymbol;

  private boolean cancelStatus;
  PrimTest pTest;
  private static ProbedivisionGUI instance;

  // Ende Attribute

  public ProbedivisionGUI() {
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 500;
    int frameHeight = 650;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Die Probedivision");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten

    checkUnicode = '\u00BB';
    checkSymbol = Character.toString(checkUnicode);
    pTest = new PrimTest();

    Hashtable<TextAttribute, Object> lEingabeAusgabe_map = new Hashtable<TextAttribute, Object>();
    lEingabeAusgabe_map.put(TextAttribute.FAMILY, "Arial");
    lEingabeAusgabe_map.put(TextAttribute.SIZE, new Integer(18));
    lEingabeAusgabe_map.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);

    lDieProbedivision.setBounds(20, 10, 288, 50);
    lDieProbedivision.setText("Die Probedivision");
    lDieProbedivision.setHorizontalAlignment(SwingConstants.LEFT);
    lDieProbedivision.setHorizontalTextPosition(SwingConstants.CENTER);
    lDieProbedivision.setForeground(Color.BLACK);

    Hashtable<TextAttribute, Object> lDieProbedivision_map = new Hashtable<TextAttribute, Object>();
    lDieProbedivision_map.put(TextAttribute.FAMILY, "Arial Black");
    lDieProbedivision_map.put(TextAttribute.SIZE, new Integer(26));
    lDieProbedivision_map.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
    lDieProbedivision.setFont(new Font(lDieProbedivision_map));
    cp.add(lDieProbedivision);

    lEingabe1.setBounds(20, 60, 100, 30);
    lEingabe1.setText("Eingabe:");
    lEingabe1.setFont(new Font(lEingabeAusgabe_map));
    cp.add(lEingabe1);

    lZahl1.setBounds(40, 100, 50, 30);
    lZahl1.setText("Zahl:");
    lZahl1.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(lZahl1);

    gegebeneZahl.setBounds(130, 100, 300, 30);
    gegebeneZahl.setText("");
    cp.add(gegebeneZahl);

    lBeispiele1.setBounds(40, 200, 80, 30);
    lBeispiele1.setText("Beispiele:");
    Hashtable<TextAttribute, Object> lBeispiele1_map = new Hashtable<TextAttribute, Object>();
    lBeispiele1_map.put(TextAttribute.FAMILY, "Arial");
    lBeispiele1_map.put(TextAttribute.SIZE, new Integer(16));
    lBeispiele1_map.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
    lBeispiele1.setFont(new Font(lBeispiele1_map));
    cp.add(lBeispiele1);

    lBestcase.setBounds(40, 240, 80, 30);
    lBestcase.setText("Best case:");
    lBestcase.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(lBestcase);

    jButton1.setBounds(130, 240, 330, 30);
    jButton1.setText("2305843009213693952");
    jButton1.setMargin(new Insets(2, 2, 2, 2));
    jButton1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        jButton1_ActionPerformed(evt);
      }
    });
    jButton1.setBackground(Color.WHITE);
    cp.add(jButton1);

    lWorstcase.setBounds(40, 280, 87, 30);
    lWorstcase.setText("Worst case:");
    lWorstcase.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(lWorstcase);

    jButton2.setBounds(130, 280, 330, 30);
    jButton2.setText("2^61-1 = 2305843009213693951");
    // 9te Mersenne Primzahl; Exponent:61	Dezimahlstellen:19	Entdeckt 1883	von Pervushin
    jButton2.setMargin(new Insets(2, 2, 2, 2));
    jButton2.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        jButton2_ActionPerformed(evt);
      }
    });
    jButton2.setBackground(Color.WHITE);
    cp.add(jButton2);

    lAusgabeText.setBounds(20, 350, 100, 30);
    lAusgabeText.setText("Ausgabe:");
    lAusgabeText.setFont(new Font(lEingabeAusgabe_map));
    cp.add(lAusgabeText);

    checkZahl.setBounds(430, 100, 30, 30);
    checkZahl.setText(checkSymbol);
    checkZahl.setFont(new Font("Arial", Font.PLAIN, 20));
    checkZahl.setMargin(new Insets(2, 2, 2, 2));
    checkZahl.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        checkZahl_ActionPerformed(evt);
      }
    });
    checkZahl.setBackground(Color.GREEN);
    checkZahl.setForeground(Color.BLACK);
    cp.add(checkZahl);

    isPrime.setBounds(40, 390, 420, 30);
    isPrime.setText("Ist eine Primzahl: ");
    isPrime.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(isPrime);

    primfaktor.setBounds(40, 430, 420, 30);
    primfaktor.setText("Besitzt den Primfaktor: ");
    primfaktor.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(primfaktor);

    lAnzahlanDivisionen.setBounds(40, 470, 420, 30);
    lAnzahlanDivisionen.setText("Anzahl der Divisionen: ");
    lAnzahlanDivisionen.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(lAnzahlanDivisionen);

    lAusgabeText.setText("Eingabe:");
    cp.add(lAusgabeText);

    lMersenneZahl.setBounds(40, 150, 120, 30);
    lMersenneZahl.setText("Mersenne-Zahl: ");
    lMersenneZahl.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(lMersenneZahl);

    MersenneExponent.setBounds(185, 140, 210, 30);
    MersenneExponent.setText("");
    cp.add(MersenneExponent);

    checkMersenne.setBackground(Color.green);
    checkMersenne.setForeground(Color.BLACK);
    checkMersenne.setFont(new Font("Arial", Font.PLAIN, 20));
    checkMersenne.setBounds(430, 150, 30, 30);
    checkMersenne.setText(checkSymbol);
    checkMersenne.setMargin(new Insets(2, 2, 2, 2));
    checkMersenne.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        checkMersenne_ActionPerformed(evt);
      }
    });
    cp.add(checkMersenne);
    l2.setBounds(170, 150, 14, 30);
    l2.setText("2");
    l2.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(l2);

    l1.setBounds(400, 150, 30, 30);
    l1.setText("- 1");
    l1.setFont(new Font("Arial", Font.PLAIN, 16));
    cp.add(l1);

    lBenoetigteZeit1.setBounds(40, 510, 420, 30);
    lBenoetigteZeit1.setFont(new Font("Arial", Font.PLAIN, 16));
    lBenoetigteZeit1.setText("Benötigte Zeit:");
    cp.add(lBenoetigteZeit1);

    bBERECHNUNGABBRECHEN.setBounds(20, 565, 200, 30);
    bBERECHNUNGABBRECHEN.setText("BERECHNUNG ABBRECHEN");
    bBERECHNUNGABBRECHEN.setMargin(new Insets(2, 2, 2, 2));
    bBERECHNUNGABBRECHEN.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        bBERECHNUNGABBRECHEN_ActionPerformed(evt);
      }
    });
    bBERECHNUNGABBRECHEN.setBackground(Color.RED);
    bBERECHNUNGABBRECHEN.setFont(new Font("Arial Black", Font.BOLD, 12));
    bBERECHNUNGABBRECHEN.setForeground(Color.BLACK);
    cp.add(bBERECHNUNGABBRECHEN);

    bBeenden.setBounds(270, 565, 200, 30);
    bBeenden.setText("Beenden");
    bBeenden.setMargin(new Insets(2, 2, 2, 2));
    bBeenden.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        bBeenden_ActionPerformed(evt);
      }
    });
    cp.add(bBeenden);

    lAusgabeText.setBounds(20, 350, 100, 30);
    lAusgabeText.setText("Ausgabe:");
    cp.add(lAusgabeText);
    cancelStatus = false;

    setVisible(true);
    // Ende Komponenten

  } // end of public Probedivision_JFrame

  public static ProbedivisionGUI getInstance() {
    if (instance == null) {
      instance = new ProbedivisionGUI();
    }
    return instance;
  }
  // Anfang Methoden

  /**
   * best run time
   */
  public void jButton1_ActionPerformed(ActionEvent evt) {
    resetAusgabe();
    BigInteger b1 = BigInteger.valueOf(2305843009213693952l);
    pTest.CalculatingNumber(b1);
  }

  /**
   * worst run time
   */
  public void jButton2_ActionPerformed(ActionEvent evt) {
    resetAusgabe();
    BigInteger b1 = BigInteger.valueOf(2305843009213693951l);
    pTest.CalculatingNumber(b1);

  }

  /**
   * gegebene Zahl
   */
  public void checkZahl_ActionPerformed(ActionEvent evt) {
    resetAusgabe();
    BigInteger b1 = gegebeneZahl.getValueAsBigInteger();
    pTest.CalculatingNumber(b1);
  }

  /**
   * mersenne prime number
   */
  public void checkMersenne_ActionPerformed(ActionEvent evt) {
    resetAusgabe();
    BigInteger base = BigInteger.valueOf(2);
    int exponent = MersenneExponent.getValueAsInt();
    BigInteger number = base.pow(exponent);
    BigInteger mersenneNumber = number.subtract(BigInteger.valueOf(1));
    pTest.CalculatingNumber(mersenneNumber);
  }

  /**
   * cancel button
   */
  public void bBERECHNUNGABBRECHEN_ActionPerformed(ActionEvent evt) {
    cancelStatus = true;
  }

  /**
   * exit button
   */
  public void bBeenden_ActionPerformed(ActionEvent evt) {
    System.exit(0);
  }

  /**
   * @return the cancel status
   */
  public boolean getCancelStatus() {
    return this.cancelStatus;
  }

  public void SetAusgabe(boolean isPrimeNew, int primfaktorNew, int AnzahlanDivisionenNew, long BenoetigteZeit1New) {
    this.isPrime.setText("Ist eine Primzahl: " + isPrimeNew);
    this.primfaktor.setText("Besitzt den Primfaktor: " + primfaktorNew);
    this.lAnzahlanDivisionen.setText("Anzahl der Divisionen " + AnzahlanDivisionenNew);
    this.lBenoetigteZeit1.setText("Benötigte Zeit " + BenoetigteZeit1New + " ms");
  }

  public void resetAusgabe() {
    SetAusgabe(false, 0, 0, 0);
  }
}
