package test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

import GUI.ProbedivisionGUI;

public class PrimTest {

    ProbedivisionGUI pgui;
    private int durchgaenge;
    private long time;
    private boolean isPrime;
    private int primfaktor;

    /**
     * Konstruktor
     */
    public void Primtest() {
        durchgaenge = 0;
        time = 0;
        isPrime = false;
        primfaktor = 0;
        this.pgui = ProbedivisionGUI.getInstance();
    }

    /**
     * @param gets a BigInteger (large number) which will be primality tested
     * @return a boolean and returns wether the BigInteger is prim number or not
     */
    public boolean bachlorThesisBig(BigInteger givenNummer )
    {
        setDurchgaenge(1);;
        int d = 0;
        if(givenNummer.equals(BigInteger.valueOf(2)))
        {
            setPrimfaktor(0);
            return true;
        }

        if((givenNummer.mod(BigInteger.valueOf(2))).equals(BigInteger.valueOf(0)))
        {
            setPrimfaktor(2);
            return false;
        }

        d = 3;

        while((BigInteger.valueOf((long) Math.pow(d,2)).compareTo(givenNummer) <= 0) )
        {
            if( givenNummer.mod(BigInteger.valueOf( (int) d)).equals(BigInteger.valueOf(0)))
            {
                setDurchgaenge(durchgaenge+1);
                setPrimfaktor(d);
                System.out.println(givenNummer.mod(BigInteger.valueOf( (long) d)));
                System.out.println( d + " is a divider of " + givenNummer );

                return false;
            }
            else
            {
                setDurchgaenge(durchgaenge+1);
            }
            d = d + 2; 

        }
        setPrimfaktor(0);
        BigInteger two = BigInteger.valueOf(2);
        BigDecimal numberofDevision =  new BigDecimal(givenNummer.sqrt()).divide(BigDecimal.valueOf(2), RoundingMode.HALF_DOWN);
        double DurchgaengeSoll = givenNummer.doubleValue() / two.doubleValue();
        BigDecimal quotient = new BigDecimal(givenNummer.sqrt()).divide(new BigDecimal(two), 3, RoundingMode.CEILING).add( BigDecimal.valueOf(0.5));
        System.out.println("Durchgäenge Soll: " + quotient + " Durchgäenge Ist: " + durchgaenge);
        return true;
    }

    /**
     * @param number primality testing 
     * messuring calculation time
     * sets the Ausgabe data to the data from the new number
     */
    public void CalculatingNumber(BigInteger number) {
        ProbedivisionGUI.getInstance().resetAusgabe();
        long start = System.currentTimeMillis();
        boolean result = bachlorThesisBig(number);
        setIsPrime(result);
        long end = System.currentTimeMillis();
        long diverence = end - start;
        ProbedivisionGUI.getInstance().SetAusgabe(result, this.getPrimfaktor(), this.getDurchgaenge(), diverence);
    }

    public int getDurchgaenge() {
        return this.durchgaenge;
    }

    public void setDurchgaenge(int durchgaenge) {
        this.durchgaenge = durchgaenge;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean getIsPrime() {
        return this.isPrime;
    }

    public void setIsPrime(boolean isPrime) {
        this.isPrime = isPrime;
    }

    public int getPrimfaktor() {
        return this.primfaktor;
    }

    public void setPrimfaktor(int primfaktor) {
        this.primfaktor = primfaktor;
    }
}
